<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cast_id')->unsigned();
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->integer('film_id')->unsigned();
            $table->foreign('film_id')->references('id')->on('film');
            $table->string('nama');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
        // $table->dropForeign(['cast_id']);
        // $table->dropColumn('cast_id');
    }
}
