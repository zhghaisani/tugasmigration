@extends ('adminlte.master')

@section('title')
Tambah Data
@endsection

@section('content')

<div>
	<form action="/cast" method="POST">
		 {{ csrf_field() }}
		 <div class="form-group">
		 	<label for="title">Nama</label>
		 	<input type="text" name="nama" class="form-control" id="title" placeholder="Masukkan Nama">
		 	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		 </div>
		 <div class="form-group">
		 	<label for="body">Umur</label>
		 	<input type="number" name="umur" class="form-control" id="body" placeholder="Masukkan Umur">
		 	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		 </div>
		  <div class="form-group">
		 	<label for="body">Biodata</label>
		 	<input type="text" name="bio" class="form-control" id="body" placeholder="Masukkan Biodata">
		 	@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
		 </div>
		 <button type="submit" class="btn btn-primary">Tambah</button>
	</form>

</div>


@endsection