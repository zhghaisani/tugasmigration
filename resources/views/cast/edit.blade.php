@extends ('adminlte.master')

@section('title')
Edit Cast {{$cast->id}}
@endsection

@section('content')

<div>
  <form action="/cast/{{$cast->id}}" method="POST">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-group">
      <label for="title">Nama</label>
      <input type="text" class="form-control" name="nama" id="title" value="{{$cast->nama}}" placeholder="Masukkan Nama" >
     <!--  @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif -->
    </div>
    <div class="form-group">
      <label for="body">Umur</label>
      <input type="number" class="form-control" name="umur" id="body" value="{{$cast->umur}}" placeholder="Masukkan Umur">
      <!-- @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif -->
    </div>
       <div class="form-group">
      <label for="body">Biodata</label>
      <input type="text" class="form-control" name="bio" id="body" value="{{$cast->bio}}"placeholder="Masukkan Biodata">
      <!-- @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif -->
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>

  </form>

</div>

@endsection